from .abstract_camera import AbstractCamera
import cv2


class UsbCamera(AbstractCamera):
    def _open(self):
        self.cap = cv2.VideoCapture(self.device)
        self.cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter.fourcc("M", "J", "P", "G"))
        self.cap.set(cv2.CAP_PROP_FPS, 10)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, self.width)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, self.height)
        self.cap.set(cv2.CAP_PROP_BUFFERSIZE, 1)

    def _capture(self):
        self.cap.grab()
        ret, photo = self.cap.read()
        if not ret:
            raise RuntimeError(f'openCV: photo not taken.\n    USB device: {self.device}')
        return photo

    def _close(self):
        self.cap.release()
        self.cap = None
