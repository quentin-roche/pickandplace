import abc
import traceback
import numpy as np
import cv2


class AbstractCamera(abc.ABC):
    num = 0

    def __init__(self, definition, device, rotation=0, flipX=False, flipY=False):
        self.device = device
        self.width, self.height = definition
        self.rotation, self.flipX, self.flipY = rotation, flipX, flipY
        self.cap = None
        self.error = None

    def _open(self):
        pass

    @abc.abstractmethod
    def _capture(self):
        pass

    @abc.abstractmethod
    def _close(self):
        pass

    def open(self):
        try:
            self._open()
        except Exception:
            traceback.print_exc()
            self.error = traceback.format_exc()

    def capture(self):
        self.num += 1
        if self.error is None:
            try:
                photo = self._capture()
                if self.flipX:
                    cv2.flip(photo, 0, photo)
                if self.flipY:
                    cv2.flip(photo, 1, photo)
                if self.rotation == "CLOCKWISE":
                    cv2.rotate(photo, cv2.ROTATE_90_CLOCKWISE, photo)
                if self.rotation == "COUNTERCLOCKWISE":
                    cv2.rotate(photo, cv2.ROTATE_90_COUNTERCLOCKWISE, photo)
                if self.rotation == "HALF_TURN":
                    cv2.rotate(photo, cv2.ROTATE_180, photo)
                cv2.putText(
                    img=photo,
                    text=str(self.num),
                    org=(self.height // 10, self.height // 10),
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=1,
                    color=(0, 0, 255),
                    thickness=2
                )
                return photo
            except Exception as error:
                traceback.print_exc()
                self.error = traceback.format_exc()

        photo = np.zeros((self.height, self.width, 3), np.uint8)
        photo[self.height // 10 * 9:, 0:self.width // 3] = (255, 0, 0)  # (B, G, R)
        photo[self.height // 10 * 9:, self.width // 3: 2 * self.width // 3] = (0, 255, 0)
        photo[self.height // 10 * 9:, 2 * self.width // 3:] = (0, 0, 255)

        self.error = f"{self.num}\n{self.error}"
        for line_index, line in enumerate(self.error.split('\n')):
            cv2.putText(
                img=photo,
                text=line,
                org=(self.height//10, self.height//10 + 50 * line_index),
                fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                fontScale=1,
                color=(255, 255, 255),
                thickness=2
            )
        self.error = None
        return photo

    def close(self):
        try:
            self._close()
        except Exception:
            traceback.print_exc()
            self.error = traceback.format_exc()
