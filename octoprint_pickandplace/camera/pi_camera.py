import os
import time

from .abstract_camera import AbstractCamera

try:
    from picamera import PiCamera as PiCameraLib
    from picamera.array import PiRGBArray
except:
    pass


class PiCamera(AbstractCamera):
    def _open(self):
        os.system('sudo service webcamd stop')
        time.sleep(3)

        self.cap = PiCameraLib(camera_num=self.device)
        self.cap.awb_mode = 'greyworld'
        self.cap.resolution = (self.width, self.height)

    def _capture(self):
        raw_capture = PiRGBArray(self.cap)
        return self.cap.capture(raw_capture, format="bgr")

    def _close(self):
        self.cap._close()
        os.system('sudo service webcamd start')
        self.cap = None
