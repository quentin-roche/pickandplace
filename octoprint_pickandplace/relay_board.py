import RPi.GPIO as GPIO


class RelayBoard:
    def __init__(self, gpio_pins):
        self.gpio_pins = gpio_pins
        GPIO.setup(self.gpio_pins, GPIO.OUT)
        for channel in range(len(gpio_pins)):
            self.off(channel)

    def on(self, channel):
        GPIO.output(self.gpio_pins[channel], 0)

    def off(self, channel):
        GPIO.output(self.gpio_pins[channel], 1)

    def set(self, channel, value):
        GPIO.output(self.gpio_pins[channel], not value)

    def number_of_channel(self):
        return len(self.gpio_pins)
