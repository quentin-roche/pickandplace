import RPi.GPIO as GPIO
import time

steps = [
    [0, 0, 0, 1],
    [0, 1, 0, 1],
    [0, 1, 0, 0],
    [0, 1, 1, 0],
    [0, 0, 1, 0],
    [1, 0, 1, 0],
    [1, 0, 0, 0],
    [1, 0, 0, 1],
]


class RotatingHead:
    def __init__(self, gpio_pins, time_step=0.001):
        self.gpio_pins = gpio_pins
        self.current_half_step = 0
        self.time_step = time_step

        GPIO.setup(self.gpio_pins, GPIO.OUT)
        self.release()
        self.output_current_step()
        self.go_to_angle(5)
        self.go_to_angle(0)

    def __run_step(self, num_half_step):
        for k in range(abs(num_half_step)):
            if num_half_step > 0:
                self.current_half_step += 1
            else:
                self.current_half_step -= 1
            self.output_current_step()
            time.sleep(self.time_step)

    def output_current_step(self):
        GPIO.output(self.gpio_pins, steps[self.current_half_step % 8])

    def release(self):
        for pin in range(4):
            GPIO.output(self.gpio_pins, [False, False, False, False])

    def get_angle(self):
        return self.current_half_step / 1024 * 90

    def go_to_angle(self, angle):
        if angle > 190:
            raise ValueError("can not go to an angle higher than 190")
        if angle < -190:
            raise ValueError("can not go to an angle lower than -190")
        to_half_step = int(angle / 90 * 1024)
        self.__run_step(to_half_step-self.current_half_step)
