import sys
import yaml



def relayDevice(args):
    import relay_board

    import RPi.GPIO as GPIO

    GPIO.setmode(GPIO.BOARD)

    print('relay device')

gcode_command_list = {
    "OCTO5000": relayDevice
}

if __name__ == "__main__":
    octo_command = sys.argv[1]
    args = sys.argv[2:]

    if octo_command not in gcode_command_list:
        raise ValueError(f"{octo_command} is not part of pick and place plugin.\nAvailable commands are: {' '.join(list(gcode_command_list.keys()))}")
    gcode_command_list[octo_command](args)

