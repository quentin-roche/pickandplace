# coding=utf-8
from __future__ import absolute_import, unicode_literals
from .camera import AbstractCamera
import uuid
import octoprint.plugin
import octoprint.printer.profile
from octoprint.events import Events
import flask
import cv2
from .rotating_head import RotatingHead
from .relay_board import RelayBoard
import RPi.GPIO as GPIO
import numpy as np

rpi_ws281x_imported = False
try:
    from rpi_ws281x import Adafruit_NeoPixel, Color
    rpi_ws281x_imported = True
except:
    pass
import time

from collections import OrderedDict

class LimitedSizeDict(OrderedDict):
    def __init__(self, *args, **kwds):
        self.size_limit = kwds.pop("size_limit", None)
        OrderedDict.__init__(self, *args, **kwds)
        self._check_size_limit()

    def __setitem__(self, key, value):
        OrderedDict.__setitem__(self, key, value)
        self._check_size_limit()

    def _check_size_limit(self):
        if self.size_limit is not None:
            while len(self) > self.size_limit:
                self.popitem(last=False)


GPIO.setmode(GPIO.BOARD)


rotating_head = RotatingHead([32, 36, 38, 40])
rotating_head.release()

relay_board = RelayBoard([31, 33, 35, 37])



class PickandplacePlugin(octoprint.plugin.StartupPlugin,
                         octoprint.plugin.TemplatePlugin,
                         octoprint.plugin.BlueprintPlugin,
                         octoprint.plugin.SettingsPlugin,
                         octoprint.plugin.AssetPlugin,
                         octoprint.plugin.EventHandlerPlugin,
                         ):

    def __init__(self):
        super().__init__()
        self.strip = None
        self.latest_position = {
            "printer": {
                "x": 0,
                "y": 0,
                "z": 0
            },
            "cameras": {}
        }
        self.photos = LimitedSizeDict(size_limit=6)
        self.cap_cameras = {}

    def hook_gcode_sending(self, comm_instance, phase, cmd, cmd_type, gcode, *args, **kwargs):
        print(cmd)
        if gcode:
            return

        if cmd[:3] != 'PNP':
            return

        command_number = int(cmd.split(' ')[0][3:])
        command_args = cmd.split(' ')[1:]

        # Relay Board
        if command_number == 0:
            for command_arg in command_args:
                channel = ord(command_arg[0]) - ord('A')
                if channel < 0 and channel >= relay_board.number_of_channel():
                    raise ValueError(f"channel for {command_arg} is unknown.\n"
                                     f"Max channel is {chr(ord('A')+relay_board.number_of_channel()-1)}")
                relay_board.set(channel, bool(int(command_arg[1:])))

        # Take photo
        elif command_number == 1:
            camera_id = command_args[0][0]
            photo_id = command_args[0][1:]
            if camera_id == 'T':
                self.take_photo('top', photo_id)
            elif camera_id == 'B':
                self.take_photo('bed', photo_id)
            elif camera_id == 'H':
                self.take_photo('head', photo_id)
            else:
                raise ValueError(f"unknown camera: {camera_id}")

        # LED strip
        elif command_number == 2:
            color = [0, 0, 0]
            start = 0
            end = self.strip.numPixels()
            for command_arg in command_args:
                if command_arg[0] == 'S':
                    start = int(command_arg[1:])
                if command_arg[0] == 'E':
                    end = int(command_arg[1:])
                if command_arg[0] == 'R':
                    color[0] = int(command_arg[1:])
                if command_arg[0] == 'G':
                    color[1] = int(command_arg[1:])
                if command_arg[0] == 'B':
                    color[2] = int(command_arg[1:])

            for i in range(start, end):
                self.strip.setPixelColor(i, Color(color[0], color[1], color[2]))
            self.strip.show()

        else:
            raise ValueError(f"unknown command: {cmd}")
        return (None,)

    def take_photo(self, cam_name, uid_photo=None):
        if uid_photo is None:
            uid_photo = str(uuid.uuid4())
        type = self._settings.get(["cameras", cam_name, "type"])
        device = self._settings.get(["cameras", cam_name, "device"])
        height = self._settings.get(["cameras", cam_name, "height"])
        width = self._settings.get(["cameras", cam_name, "width"])
        rotation = self._settings.get(["cameras", cam_name, "rotation"])
        flip_x = self._settings.get(["cameras", cam_name, "flip_x"])
        flip_y = self._settings.get(["cameras", cam_name, "flip_y"])

        if cam_name not in self.cap_cameras:
            print(AbstractCamera.__subclasses__())
            print(list(AbstractCamera.__subclasses__()))
            added = False
            for subclass in AbstractCamera.__subclasses__():
                if subclass.__name__.upper().startswith(type.upper()):
                    cap_camera = subclass((width, height), device, rotation, flip_x, flip_y)
                    cap_camera.open()
                    self.cap_cameras[cam_name] = cap_camera
                    added = True
            if not added:
                ValueError(f'Type of camera {type} is not supported')
        print(self.cap_cameras)
        cap = self.cap_cameras[cam_name]

        photo = cap.capture()

        self.photos[uid_photo] = photo
        print(f'photo taken from {cam_name}. uid is {uid_photo}.')

        self._event_bus.fire(Events.PLUGIN_PICKANDPLACE_PHOTO_TAKEN, {
            "uid": uid_photo
        })
        return photo, uid_photo

    def on_startup(self, *args, **kwargs):
        self._logger.info("Pick and place")

        if rpi_ws281x_imported:
            # Led strip
            self.strip = Adafruit_NeoPixel(
                self._settings.get(["led_strip", "length"]),
                self._settings.get(["led_strip", "pin"]),
                self._settings.get(["led_strip", "frequency"]),
                self._settings.get(["led_strip", "dma"]),
                self._settings.get(["led_strip", "inverted"]),
                255
            )
            self.strip.begin()
            for i in range(0, self.strip.numPixels(), 1):
                self.strip.setPixelColor(i, Color(0, 0, 0))
            self.strip.show()




    def get_template_configs(self):
        return [
            dict(type="tab", custom_bindings=False)
        ]

    def get_assets(self):
        return dict(
            js=[
                "js/settings.js",
                "js/simple-action.js",
            ]
        )

    def get_settings_defaults(self):
        return {
            "needle_height": 20,
            "focus_height": 27,
            "pixels_per_mm": 85,
            "delta_needle_x": 1.3,
            "delta_needle_y": 45.3,
            "cameras": {
                "top": {
                    "type": "pi",
                    "device": 0,
                    "width": 3280,
                    "height": 2464,
                    "rotation": "NO",
                    "flip_x": False,
                    "flip_y": False,
                },
                "head": {
                    "type": "usb",
                    "device": "/dev/v4l/by-path/platform-3f980000.usb-usb-0:1.3:1.0-video-index0",
                    "width": 2048,
                    "height": 1536,
                    "rotation": "NO",
                    "flip_x": False,
                    "flip_y": False,
                    "focus_z": 10,
                    "camera_delta_x": 0,
                    "camera_delta_y": 0,
                    "printer_x": 0,
                    "printer_y": 0,
                    "mm_per_pixel": 0.01
                },
                "bed": {
                    "type": "usb",
                    "device": "/dev/v4l/by-path/platform-3f980000.usb-usb-0:1.2:1.0-video-index0",
                    "width": 2048,
                    "height": 1536,
                    "rotation": "NO",
                    "flip_x": False,
                    "flip_y": False,
                    "focus_z": 10,
                    "camera_delta_x": 0,
                    "camera_delta_y": 0,
                    "printer_x": 0,
                    "printer_y": 0,
                    "mm_per_pixel": 0.01,
                }
            },
            "led_strip": {
                "pin": 10,
                "length": 36,
                "frequency": 800000,
                "dma": 10,
                "inverted": False,
            }
        }


    @octoprint.plugin.BlueprintPlugin.route("/photo/<uid>", methods=["GET"])
    def route_photo(self, uid):
        if uid not in self.photos:
            print(f'photo {uid} is not available')
            print(list(self.photos.keys()))
            flask.abort(flask.Response('photo not available'))
            return
        retval, buffer = cv2.imencode('.jpg', self.photos[uid])
        response = flask.make_response(buffer.tobytes())
        response.headers.set('Content-Type', 'image/jpeg')
        return response

    @octoprint.plugin.BlueprintPlugin.route("/rotate-head", methods=["POST"])
    def route_rotate_head(self):
        angle = flask.request.json['to']
        rotating_head.go_to_angle(float(angle))
        return flask.Response(status=200)

    @octoprint.plugin.BlueprintPlugin.route("/move", methods=["POST"])
    def route_move(self):
        type = flask.request.json['type']
        move_x = flask.request.json.get('mmX')
        move_y = flask.request.json.get('mmY')
        move_z = flask.request.json.get('mmZ')

        self._event_bus.fire(Events.PLUGIN_PICKANDPLACE_MOVE_ASKED, {
            "type": "type",
            "move_x": "move_x",
            "move_y": "move_y",
            "move_z": "move_z",
        })

        if type == 'relative':
            self._printer.commands('G91;')
        elif type != 'absolute':
            raise ValueError('Type of movement should be relative or absolute')

        move_command = 'G00'
        if move_x is not None:
            move_command += ' X'+str(move_x)
        if move_y is not None:
            move_command += ' Y'+str(move_y)
        if move_z is not None:
            move_command += ' Z'+str(move_z)
        move_command += ' F6000;'
        print(move_command)
        self._printer.commands(move_command)

        if type == 'relative':
            self._printer.commands('G90;')
        self._printer.commands('M114;')

        return flask.Response(status=200)

    def on_event(self, event, payload):
        print(f"event: {event}\n   {payload}")
        if event == "PositionUpdate":
            self.latest_position["printer"]["x"] = payload["x"]
            self.latest_position["printer"]["y"] = payload["y"]
            self.latest_position["printer"]["z"] = payload["z"]
            print(list(self._settings.get(["cameras"]).keys()))
            for camera in self._settings.get(["cameras"]).keys():
                photo, uid_photo = self.take_photo(camera)
                self.latest_position["cameras"][camera] = {
                    "uid": uid_photo,
                    "height": photo.shape[0],
                    "width": photo.shape[1]
                }
            self._event_bus.fire(Events.PLUGIN_PICKANDPLACE_LATEST_POSITION_PHOTO_TAKEN, self.latest_position)
        if event == "SettingsUpdated":
            for cap_camera in self.cap_cameras.items():
                cap_camera.close()
            cap_cameras = {}

    @octoprint.plugin.BlueprintPlugin.route("/latest_position-needle-center/<camera_name>", methods=["GET"])
    def get_camera_needle_center(self, camera_name):
        photo_uid = self.latest_position["cameras"][camera_name]["uid"]
        photo_with_indicator_uid = f"{photo_uid}_needle_center"

        img = self.photos[photo_uid]
        img_blurred = cv2.medianBlur(img, 5)
        img_blurred = cv2.cvtColor(img_blurred, cv2.COLOR_BGR2GRAY)

        circles = cv2.HoughCircles(
            img_blurred,
            cv2.HOUGH_GRADIENT,
            1,
            20,
            param1=50,
            param2=30,
            minRadius=0,
            maxRadius=0)

        print(circles)
        print("circles")

        if circles is None:
            return flask.jsonify({
                "printer": {
                    "x": self.latest_position["printer"]["x"],
                    "y": self.latest_position["printer"]["y"],
                    "z": self.latest_position["printer"]["z"],
                },
                "object": None,
                "photo": {
                    "uid": photo_uid,
                    "height": img.shape[0],
                    "width": img.shape[1]
                },
                "photo_with_indicator_uid": None
            })
        circles = np.uint16(np.around(circles))

        img_with_circle = img.copy()
        cv2.circle(img_with_circle, (circles[0][0][0], circles[0][0][1]), circles[0][0][2], (0, 255, 0), 2)
        cv2.circle(img_with_circle, (circles[0][0][0], circles[0][0][1]), 2, (0, 0, 255), 3)
        self.photos[photo_with_indicator_uid] = img_with_circle

        return flask.jsonify({
            "printer": {
                "x": self.latest_position["printer"]["x"],
                "y": self.latest_position["printer"]["y"],
                "z": self.latest_position["printer"]["z"],
            },
            "object": {
                "x": int(circles[0][0][0]),
                "y": int(circles[0][0][1]),
                "size": int(circles[0][0][1]),
            },
            "photo": {
                "uid": photo_uid,
                "height": img.shape[0],
                "width": img.shape[1]
            },
            "photo_with_indicator": {
                "uid": photo_with_indicator_uid,
                "height": img_with_circle.shape[0],
                "width": img_with_circle.shape[1]
            },
        })


def _register_custom_events(*args, **kwargs):
    return ["move_asked", "photo_taken", 'latest_position_photo_taken']


__plugin_name__ = "Pick and place"
__plugin_version__ = "1.0.0"
__plugin_description__ = "A quick \"Hello World\" example plugin for OctoPrint"
__plugin_pythoncompat__ = ">=2.7,<4"
__plugin_implementation__ = PickandplacePlugin()
__plugin_hooks__ = {
    "octoprint.comm.protocol.gcode.sending": __plugin_implementation__.hook_gcode_sending,
    "octoprint.events.register_custom_events": _register_custom_events,
}
