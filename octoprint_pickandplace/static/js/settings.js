function findLineByLeastSquares(values_x, values_y) {
    var x_sum = 0;
    var y_sum = 0;
    var xy_sum = 0;
    var xx_sum = 0;
    var count = 0;

    /*
     * The above is just for quick access, makes the program faster
     */
    var x = 0;
    var y = 0;
    var values_length = values_x.length;

    if (values_length !== values_y.length) {
        throw new Error('The parameters values_x and values_y need to have same size!');
    }

    /*
     * Above and below cover edge cases
     */
    if (values_length === 0) {
        return [ [], [] ];
    }

    /*
     * Calculate the sum for each of the parts necessary.
     */
    for (let i = 0; i< values_length; i++) {
        x = values_x[i];
        y = values_y[i];
        x_sum+= x;
        y_sum+= y;
        xx_sum += x*x;
        xy_sum += x*y;
        count++;
    }

    /*
     * Calculate m and b for the line equation:
     * y = x * m + b
     */
    var m = (count*xy_sum - x_sum*y_sum) / (count*xx_sum - x_sum*x_sum);
    var b = (y_sum/count) - (m*x_sum)/count;

    return {
        m,
        b
    }
}

$(function() {
    console.log('ici0')
    function PickAndPlaceSettingsViewModel(parameters) {
        var self = this;
        self.settings = parameters[0];

        self.autoUpdateImages = true;
        self.markNeedlePosition = ko.observable(null);

        self.moveLengthInput = ko.observable();
        self.moveLengthInput(-1)

        self.moveLength = ko.observable(.1);

        self.moveLengthInput.subscribe((moveLengthInputValue) => {
            self.moveLength(Math.round((10**moveLengthInputValue)*10)/10);
        });

        self.moveXInc = async () => {await self.moveRelative( self.moveLength(), 0, 0)}
        self.moveXDec = async () => {await self.moveRelative( -self.moveLength(), 0, 0)}
        self.moveYInc = async () => {await self.moveRelative(0, self.moveLength(), 0)}
        self.moveYDec = async () => {await self.moveRelative(0, -self.moveLength(), 0)}
        self.moveZInc = async () => {await self.moveRelative(0, 0, self.moveLength())}
        self.moveZDec = async () => {await self.moveRelative(0, 0, -self.moveLength())}

        let resolvePromisePosition = null;
        self.getPosition = () => {
            OctoPrint.control.sendGcode(`M114`);
            return new Promise(resolve => {
                resolvePromisePosition = resolve;
            })
        }

        self.moveRelative = async (mmX, mmY, mmZ, wait_for_photo=true) => self.move('relative', mmX, mmY, mmZ, wait_for_photo)
        self.moveAbsolute = async (mmX, mmY, mmZ, wait_for_photo=true) => self.move('absolute', mmX, mmY, mmZ, wait_for_photo)
        self.move = async (type, mmX, mmY, mmZ, wait_for_photo) => {
            await fetch('/plugin/pickandplace/move', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    type,
                    mmX,
                    mmY,
                    mmZ
                })
            });
            if (wait_for_photo) {
                console.log('waiting for LatestPhotoTaken...');
                const result =  await self.waitForLatestPhotoTaken();
                console.log('waiting done');
                return result
            }
        }

        self.onEventPositionUpdate = (position) => {
            if (resolvePromisePosition !== null) {
                resolvePromisePosition(position)
            }
        }

        self.onEventplugin_pickandplace_move_asked = () => {
            self.canvasCameraBedClear();
            self.canvasCameraHeadClear();
        }

        let latestPhotoTakenResolves = [];
        self.onEventplugin_pickandplace_latest_position_photo_taken  = (payload) => {
            console.log(payload)
            self.canvasCameraBedDrawImage(payload.cameras.bed.uid);
            self.canvasCameraHeadDrawImage(payload.cameras.head.uid);
            latestPhotoTakenResolves.forEach(latestPhotoTakenResolve => latestPhotoTakenResolve());
            latestPhotoTakenResolves = [];
        }
        self.waitForLatestPhotoTaken = async () => {
            return new Promise(resolve => {
                latestPhotoTakenResolves.push(resolve);
            })
        }

        /* Bed camera calibration ----------------------------------------------------------------------------------- */
        const canvasCameraBed = document.getElementById('canvas-calibration-camera-bed');
        canvasCameraBed.style.width = '100%'
        self.canvasCameraBedClear = () => {
            const ctx = canvasCameraBed.getContext('2d');
            ctx.fillStyle = "#8d1e1e";
            ctx.rect(0,0 , canvasCameraBed.width, canvasCameraBed.height)
            ctx.fill();
        };
        const imgBed = new Image;
        imgBed.onload = function(){
            const ctx = canvasCameraBed.getContext('2d');
            canvasCameraBed.width = imgBed.width;
            canvasCameraBed.height = imgBed.height;
            ctx.drawImage(imgBed,0,0); // Or at whatever offset you like
            ctx.strokeStyle = "#ff0000";
            ctx.lineWidth = 5;
            ctx.beginPath();
            ctx.moveTo(0, imgBed.height/2);
            ctx.lineTo(imgBed.width, imgBed.height/2);
            ctx.moveTo(imgBed.width/2, 0);
            ctx.lineTo(imgBed.width/2, imgBed.height);
            ctx.stroke();
        };
        self.canvasCameraBedDrawImage = (uid) => {
            self.canvasCameraBedClear()
            console.log(uid)
            imgBed.src = `/plugin/pickandplace/photo/${uid}`;
        }
        self.getBedCameraNeedleCenter = async () => {
            return (await fetch('/plugin/pickandplace/latest_position-needle-center/bed', {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json'
                }
            })).json();
        }
        self.calibrateBedCamera = async () => {
            let mm_per_pixel;
            moves = [
                [0,0],
                [1,0],
                [1,1],
                [0,1],
                [-1,1],
                [-1,0],
                [-1,-1],
                [0,-1],
                [1,-1],
                [0,0],
            ]
            positions = []
            for (const ratio of [.5, 1]) {
                for (const move of moves) {
                    positions.push([
                        move[0] * ratio,
                        move[1] * ratio
                    ])
                }
            }
            self.autoUpdateImages = false;

            const base_position_printer = await self.getPosition()

            const printerX = [];
            const printerY = [];
            const objectX = [];
            const objectY = [];

            for (position of positions) {
                await self.moveAbsolute(
                    base_position_printer.x + position[0],
                    base_position_printer.y + position[1],
                    null,
                    )
                const result = await self.getBedCameraNeedleCenter();
                if (result.object !== null) {
                    self.canvasCameraBedDrawImage(result.photo_with_indicator.uid)
                    printerX.push(result.printer.x)
                    printerY.push(result.printer.y)
                    objectX.push(result.object.x - result.photo.width/2)
                    objectY.push(result.object.y - result.photo.height/2)
                } else {
                    console.log('circle not found')
                    self.canvasCameraBedDrawImage(result.photo)
                }
            }

            console.log([
                printerX,
                printerY,
                objectX,
                objectY,
            ])
            const leastSquareX = findLineByLeastSquares(objectX, printerX);
            const leastSquareY = findLineByLeastSquares(objectY, printerY);

            console.log(leastSquareX)
            console.log(leastSquareY)

            await self.moveAbsolute(leastSquare.b ,null, null)

            self.autoUpdateImages = true;

            let result;
            do {
                 result = await self.getBedCameraNeedleCenter();
            } while (result.object.y !== null)
            self.settings.settings.plugins.pickandplace.cameras.bed.mm_per_pixel(leastSquareX.m);
            self.settings.settings.plugins.pickandplace.cameras.bed.focus_z(result.printer.z);
            self.settings.settings.plugins.pickandplace.cameras.bed.needle_x(result.printer.x);
            self.settings.settings.plugins.pickandplace.cameras.bed.needle_delta_y(
                (result.object.y - result.photo.width/2) * leastSquareX.m);
        }
        self.goToCalibratedBed = async () => {
            await self.moveAbsolute(
                self.settings.settings.plugins.pickandplace.cameras.bed.needle_x(),
                null,
                null,
                false
            )
            await self.moveAbsolute(
                null,
                null,
                self.settings.settings.plugins.pickandplace.cameras.bed.focus_z(),
            )
        }

        /* Head camera calibration ---------------------------------------------------------------------------------- */

        const canvasCameraHead = document.getElementById('canvas-calibration-camera-head');
        canvasCameraHead.style.width = '100%'
        self.canvasCameraHeadClear = () => {
            const ctx = canvasCameraBed.getContext('2d');
            ctx.fillStyle = "#8d1e1e";
            ctx.rect(0,0 , canvasCameraBed.width, canvasCameraBed.height)
            ctx.fill();
        };
        const imgHead = new Image;
        imgHead.onload = function(){
            const ctx = canvasCameraHead.getContext('2d');
            canvasCameraHead.width = imgHead.width;
            canvasCameraHead.height = imgHead.height;
            ctx.drawImage(imgHead,0,0); // Or at whatever offset you like
            ctx.strokeStyle = "#ff0000";
            ctx.lineWidth = 5;
            ctx.beginPath();
            ctx.moveTo(0, imgBed.height/2);
            ctx.lineTo(imgBed.width, imgBed.height/2);
            ctx.moveTo(imgBed.width/2, 0);
            ctx.lineTo(imgBed.width/2, imgBed.height);
            ctx.stroke();
        };
        self.canvasCameraHeadDrawImage = (uid) => {
            self.canvasCameraBedClear()
            imgHead.src = `/plugin/pickandplace/photo/${uid}`;
        }
        self.getHeadCameraNeedleCenter = async () => {
            return (await fetch('/plugin/pickandplace/head-needle-center', {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json'
                }
            })).json();
        }
        self.markMadeHeadCamera = async () => {
            self.markNeedlePosition(await self.getPosition());
            await self.goToCalibratedHead();
        }
        self.calibrateHeadCamera = async () => {
            let mm_per_pixel;
            positions = [
                [0,0],
                [.5,0],
                [1,0],
                [.5,0],
                [0,0],
                [-.5,0],
                [-1,0],
                [.5,0],
                [0,0],
            ]
            self.autoUpdateImages = false;
            for (let loop=0; loop < 3; loop+=1) {
                const center_head_position = await self.getPosition()

                const xBed = [];
                const xCamera = [];

                for (position of positions) {
                    await self.moveAbsolute(
                        center_head_position.x + position[0],
                        center_head_position.y + position[1],
                        null)
                    const bed = await self.getPosition();
                    const needle = await self.getHeadCameraNeedleCenter();
                    self.canvasCameraHeadDrawImage(needle.photo_uid)
                    if (needle.x !== null) {
                        xBed.push(bed.x)
                        xCamera.push(needle.x - canvasCameraHead.width/2)
                    }
                }
                const result = findLineByLeastSquares(xCamera, xBed);
                mm_per_pixel = result.m;
                await self.moveAbsolute(result.b ,null, null)
            }
            self.autoUpdateImages = true;

            const printer_position = await self.getPosition()
            while (true) {
                const needle = await self.getHeadCameraNeedleCenter();
                if (needle.y !== null) {
                    break
                }
            }
            self.settings.settings.plugins.pickandplace.cameras.head.mm_per_pixel(mm_per_pixel);
            self.settings.settings.plugins.pickandplace.cameras.head.focus_z(printer_position.z);
            self.settings.settings.plugins.pickandplace.cameras.head.needle_delta_x(
                printer_position.x - self.markNeedlePosition.x);
            self.settings.settings.plugins.pickandplace.cameras.head.needle_delta_y(
                printer_position.y - self.markNeedlePosition.y);
        }
        self.goToCalibratedHead = async () => {
            await self.moveAbsolute(
                null,
                null,
                self.settings.settings.plugins.pickandplace.cameras.head.focus_z(),
            )
            await self.moveRelative(
                self.settings.settings.plugins.pickandplace.cameras.head.needle_delta_x(),
                self.settings.settings.plugins.pickandplace.cameras.head.needle_delta_y(),
                null,
                null
            )
        }
    }

    OCTOPRINT_VIEWMODELS.push([
        // This is the constructor to call for instantiating the plugin
        PickAndPlaceSettingsViewModel,

        // This is a list of dependencies to inject into the plugin, the order which you request
        // here is the order in which the dependencies will be injected into your view model upon
        // instantiation via the parameters argument
        ["settingsViewModel"],

        // Finally, this is the list of selectors for all elements we want this view model to be bound to.
        ["#settings_plugin_pickandplace"]
    ]);
});
