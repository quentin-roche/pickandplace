$(function() {
    function PickAndPlaceMoveHeadViewModel(parameters) {

        console.log("PickAndPlaceMoveHeadViewModel loadead")
        var self = this;
        self.settings = parameters[0];


        self.moveLengthInput = ko.observable();
        self.moveLengthInput(-1)

        self.moveLength = ko.observable(.1);

        self.moveLengthInput.subscribe((moveLengthInputValue) => {
            self.moveLength(Math.round((10**moveLengthInputValue)*10)/10);
        });

        self.moveXInc = async () => {await self.moveRelativeXY( self.moveLength(), 0, 0)}
        self.moveXDec = async () => {await self.moveRelativeXY( -self.moveLength(), 0, 0)}
        self.moveYInc = async () => {await self.moveRelativeXY(0, self.moveLength(), 0)}
        self.moveYDec = async () => {await self.moveRelativeXY(0, -self.moveLength(), 0)}
        self.moveZInc = async () => {await self.moveRelativeXY(0, 0, self.moveLength(), 0)}
        self.moveZDec = async () => {await self.moveRelativeXY(0, 0, -self.moveLength(), 0)}


        // Move
        self.moveRelativeXY = async (mmX, mmY, mmZ) => {
            await fetch('/plugin/pickandplace/move', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    type: 'relative',
                    mmX,
                    mmY,
                    mmZ
                })
            });
        }
    }

    OCTOPRINT_VIEWMODELS.push([
        // This is the constructor to call for instantiating the plugin
        PickAndPlaceMoveHeadViewModel,

        // This is a list of dependencies to inject into the plugin, the order which you request
        // here is the order in which the dependencies will be injected into your view model upon
        // instantiation via the parameters argument
        ["settingsViewModel"],

        // Finally, this is the list of selectors for all elements we want this view model to be bound to.
        ["#pickandplace_move_head"]
    ]);
});
