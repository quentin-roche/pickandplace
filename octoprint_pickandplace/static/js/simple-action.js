$(function() {

    function PickAndPlaceViewModel(parameters) {
        var self = this;

        self.settings = parameters[0];
        console.log('here')

        self.processStage  = ko.observable();
        self.processStage(0)


        // Vacuum
        self.vacuumOn = () => fetch('/plugin/pickandplace/vacuum-on', {
            method: 'post',
            body: {}
        });
        self.vacuumOff = () => fetch('/plugin/pickandplace/vacuum-off', {
            method: 'post',
            body: {}
        });

        // Head rotation
        self.headAngle = ko.observable();
        self.headRotate = () => fetch('/plugin/pickandplace/rotate-head', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                to: parseFloat(self.headAngle())
            })
        });

        // PartImage
        let partImageLoaded = false;
        const partImage = new Image();
        let partImageRelativePosition = [0,0]
        let boardImageLoaded = false;
        const boardImage = new Image();
        let boardImageRelativePosition = [0,0]
        partImage.onload = () => {
            partImageRelativePosition = [0,0]
            partImageLoaded = true;
            drawCanvas();
        }
        const reloadPartImage = () => {
            partImageLoaded = false;
            partImage.src = '/plugin/pickandplace/take-picture?' + new Date().getTime();
        }

        // BoardImage
        boardImage.onload = () => {
            boardImageRelativePosition = [0,0]
            boardImageLoaded = true;
            drawCanvas();
        }
        const reloadBoardImage = () => {
            console.log('reloadBoardImage')
            boardImageLoaded = false;
            boardImage.src = '/plugin/pickandplace/take-picture?' + new Date().getTime();
        }

        // Move
        self.moveRelativeXY = async (mmX, mmY) => {
            console.log(self.processStage())
            if (self.processStage() === 0) {
                partImageRelativePosition[0] -= mmX * self.settings.settings.plugins.pickandplace.pixels_per_mm();
                partImageRelativePosition[1] += mmY * self.settings.settings.plugins.pickandplace.pixels_per_mm();
            } else if(self.processStage() === 1) {
                boardImageRelativePosition[0] -= mmX * self.settings.settings.plugins.pickandplace.pixels_per_mm();
                boardImageRelativePosition[1] += mmY * self.settings.settings.plugins.pickandplace.pixels_per_mm();
            }
            drawCanvas();
            await fetch('/plugin/pickandplace/move', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    type: 'relative',
                    mmX,
                    mmY
                })
            });
        }

        self.moveLength = ko.observable();
        self.moveLength(.1)
        self.moveUp = async () => {await self.moveRelativeXY(0, self.moveLength())}
        self.moveDown = async () => {await self.moveRelativeXY(0, -self.moveLength())}
        self.moveLeft = async () => {await self.moveRelativeXY(-self.moveLength(), 0)}
        self.moveRight = async () => {await self.moveRelativeXY(self.moveLength(), 0)}

        const canvas = $('#display-canvas')[0]
        canvas.style.width = '100%'

        self.partOpacity = ko.observable();
        self.partOpacity(.5);

        canvas.addEventListener('click', async event => {
            let rect = canvas.getBoundingClientRect();
            let x = ((event.clientX - rect.left) / rect.width - .5) * canvas.width;
            let y = ((event.clientY - rect.top) / rect.height - .5) * canvas.height;
            await self.moveRelativeXY(
                x /self.settings.settings.plugins.pickandplace.pixels_per_mm(),
                -y /self.settings.settings.plugins.pickandplace.pixels_per_mm()
            )
        })

        const drawCanvas = () => {
            const ctx = canvas.getContext("2d");
            canvas.width = partImage.width
            canvas.height = partImage.height

            ctx.fillStyle = '#03a1bf'
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            console.log(boardImageLoaded)
            if (boardImageLoaded) {
                ctx.drawImage(boardImage, boardImageRelativePosition[0], boardImageRelativePosition[1], canvas.width, canvas.height)
                ctx.globalAlpha = self.partOpacity();
                ctx.drawImage(partImage, partImageRelativePosition[0], partImageRelativePosition[1], canvas.width, canvas.height)
                ctx.globalAlpha = 1;
            } else {
                ctx.drawImage(partImage, partImageRelativePosition[0], partImageRelativePosition[1], canvas.width, canvas.height)
            }

            ctx.lineWidth = 5;
            ctx.strokeStyle = 'rgba(255,0,0,0.5)'
            ctx.beginPath();
            ctx.moveTo(0, canvas.height/2)
            ctx.lineTo(canvas.width, canvas.height/2)
            ctx.moveTo(canvas.width/2, 0)
            ctx.lineTo(canvas.width/2, canvas.height)
            ctx.stroke()
        }

        reloadPartImage();
        self.takePhoto = () => {
            if (self.processStage() === 0) {
                reloadPartImage()
            } else if (self.processStage() === 1) {
                reloadBoardImage()
            }
        }

        self.nextStage = async () => {
            if (self.processStage() === 0) {
                await self.vacuumOn()
                await self.moveRelativeXY(
                    -self.settings.settings.plugins.pickandplace.delta_needle_x(),
                    -self.settings.settings.plugins.pickandplace.delta_needle_y()
                )
                console.log('pick');
                await self.moveRelativeXY(
                    self.settings.settings.plugins.pickandplace.delta_needle_x(),
                    self.settings.settings.plugins.pickandplace.delta_needle_y()
                )
            }

            if (self.processStage() === 1) {
                await self.moveRelativeXY(
                    -self.settings.settings.plugins.pickandplace.delta_needle_x(),
                    -self.settings.settings.plugins.pickandplace.delta_needle_y()
                )
                setTimeout(async () => {
                    await self.vacuumOff()
                    console.log('place');
                    await self.moveRelativeXY(
                        self.settings.settings.plugins.pickandplace.delta_needle_x(),
                        self.settings.settings.plugins.pickandplace.delta_needle_y()
                    )
                },2000);
            }
            self.processStage((self.processStage() + 1)%3)
        }

        const update_canvas = () => {

           const context = canvas.getContext("2d");
        };

        $(window).resize(update_canvas);

    }


    OCTOPRINT_VIEWMODELS.push([
        // This is the constructor to call for instantiating the plugin
        PickAndPlaceViewModel,

        // This is a list of dependencies to inject into the plugin, the order which you request
        // here is the order in which the dependencies will be injected into your view model upon
        // instantiation via the parameters argument
        ["settingsViewModel"],

        // Finally, this is the list of selectors for all elements we want this view model to be bound to.
        ["#tab_plugin_pickandplace"]
    ]);
});
